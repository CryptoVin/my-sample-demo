package com.sqlite.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sqlite.dao.StudentDao;
import com.sqlite.entities.Student;
import com.sqlite.service.IStudentService;

@Service
public class StudentService implements IStudentService{

	@Autowired
	StudentDao studentdao;
	
	
	@Override
	public void save(Student student) {
		// TODO Auto-generated method stub
		Student addme = studentdao.save(student);
		System.out.println(addme);
		
	}

}
