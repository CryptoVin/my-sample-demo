package com.sqlite.service;

import com.sqlite.entities.Student;

public interface IStudentService {

	public void save(Student student);
}
