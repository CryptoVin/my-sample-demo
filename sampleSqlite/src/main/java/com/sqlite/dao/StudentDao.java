package com.sqlite.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.sqlite.entities.Student;

@RepositoryRestResource
public interface StudentDao extends CrudRepository<Student, Long>{

	Student save(Student s);
}
